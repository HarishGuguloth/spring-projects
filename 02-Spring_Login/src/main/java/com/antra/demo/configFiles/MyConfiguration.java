package com.antra.demo.configFiles;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@Configuration
@ComponentScan(basePackages="com.antra")
public class MyConfiguration {
	
	@Bean
	public ViewResolver viewResolver()
	{
		InternalResourceViewResolver view=new InternalResourceViewResolver();
		view.setPrefix("/WEB-INF/views/");
		view.setSuffix(".jsp");
		return view;
		
	}
	
	@Bean
	public DataSource dataSource()
	{
		DriverManagerDataSource dd=new DriverManagerDataSource();
		dd.setDriverClassName("com.mysql.cj.jdbc.Driver");
		dd.setUrl("jdbc:mysql://localhost:3306/test");
		dd.setUsername("root");
		dd.setPassword("antra");
		return dd;
	}
	@Bean
	public JdbcTemplate jdbc()
	{
		JdbcTemplate jtemp=new JdbcTemplate(dataSource());
		return jtemp;
		
	}
	

}
