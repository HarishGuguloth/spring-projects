package com.antra.demo.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.antra.demo.service.impl.LoginServiceImpl;

@Controller
public class LoginController {
	
	@Autowired
	LoginServiceImpl logins;
	
	
	@GetMapping(value="login")
	public ModelAndView login()
	{
	     ModelAndView mm=new ModelAndView("Login");
	     return mm;
		
	}
	
	@PostMapping("verify")
	
	public ModelAndView verifyLogin(HttpServletRequest req,HttpServletResponse res,Model model)
	{
		ModelAndView mav;
		boolean b=logins.verifyDetails(req.getParameter("uname"),req.getParameter("pword"));
		model.addAttribute("user",req.getParameter("uname"));
		if(b==true)
		{
			mav=new ModelAndView("Success");
		}
		else
		{
			mav=new ModelAndView("Fail");
			
		}
		return mav;
		
		
	}
	

}
