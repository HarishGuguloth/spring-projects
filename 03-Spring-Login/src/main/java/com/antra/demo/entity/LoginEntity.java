package com.antra.demo.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Login")

public class LoginEntity {
	
	
	@Id

	private String uname;
	
	private String Pword;
	
	private int active;

	public String getUname() {
		return uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}

	public String getPword() {
		return Pword;
	}

	public void setPword(String pword) {
		Pword = pword;
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}
	
	
	
	

}
