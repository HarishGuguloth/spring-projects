package com.antra.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.antra.demo.dao.service.impl.LoginServiceImpl;

@Controller
public class LoginContoller {
	@Autowired
	LoginServiceImpl loginServiceImpl;
	
	
	@GetMapping("/login")
	public String loginPage()
	{
		return "Login";
	}
	
	
	@PostMapping("verify")
	public String verifyLogin(@RequestParam(value="uname") String username,@RequestParam(value="pword" ) String password,Model model )
	{
		boolean b=loginServiceImpl.VerifyUsers(username, password);
		model.addAttribute("user", username);
		if(b==true)
		{
			return "Success";
		}
		else
		{
			return "Fail";
		}
			
		
		
	}

}
