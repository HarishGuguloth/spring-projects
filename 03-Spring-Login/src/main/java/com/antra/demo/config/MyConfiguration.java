package com.antra.demo.config;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@Configuration
@ComponentScan(basePackages="com.antra")
public class MyConfiguration 
{
	@Bean
	public ViewResolver viewResolver()
	{
		InternalResourceViewResolver view=new InternalResourceViewResolver();
		view.setPrefix("/WEB-INF/views/");
		view.setSuffix(".jsp");
		return view;
	}
	@Bean
	public DataSource dataSource()
	{
		DriverManagerDataSource ds=new DriverManagerDataSource();
		ds.setDriverClassName("com.mysql.cj.jdbc.Driver");
		ds.setUrl("jdbc:mysql://localhost:3306/test");
		ds.setUsername("root");
		ds.setPassword("antra");
		return ds;
	}
	@Bean
	public HibernateJpaVendorAdapter jpaVendor()
	{
		HibernateJpaVendorAdapter hh=new HibernateJpaVendorAdapter();
		hh.setGenerateDdl(true);
		hh.setShowSql(true);
		return hh;
		
	}
	@Bean
	public LocalContainerEntityManagerFactoryBean factoryBean()
	{
		 LocalContainerEntityManagerFactoryBean ll=new  LocalContainerEntityManagerFactoryBean ();
		ll.setDataSource(dataSource());
		ll.setJpaVendorAdapter(jpaVendor());
		ll.setPackagesToScan("com.antra");
		return ll;
		
	}
	@Bean
	public JpaTransactionManager manager()
	{
		JpaTransactionManager jj=new JpaTransactionManager();
		jj.setEntityManagerFactory(factoryBean().getObject());
		return jj;
		
		
	}
	

}
