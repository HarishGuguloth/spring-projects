package com.antra;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Emps")
public class EmployeeEntity 
{
	@Id
	private Integer empId;
	private String empName;
	private Double salary;
	private Integer deptNumber;
	public Integer getEmpId() {
		return empId;
	}
	public void setEmpId(Integer empId) {
		this.empId = empId;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public Double getSalary() {
		return salary;
	}
	public void setSalary(Double salary) {
		this.salary = salary;
	}
	public Integer getDeptNumber() {
		return deptNumber;
	}
	public void setDeptNumber(Integer deptNumber) {
		this.deptNumber = deptNumber;
	}

}
