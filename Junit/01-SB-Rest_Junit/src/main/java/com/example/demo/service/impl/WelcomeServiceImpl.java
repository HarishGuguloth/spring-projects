package com.example.demo.service.impl;

import org.springframework.stereotype.Service;

import com.example.demo.model.User;
import com.example.demo.service.WelcomeService;

@Service
public class WelcomeServiceImpl implements WelcomeService {

	@Override
	public String getWelcomeMsg() {
		
		return "welcome to my world";
	}
	
	@Override
	public boolean addUser(User u) 
	{
		//after some logic
		
		return true;
	}
	@Override
	public boolean updateUser(User user) {
		//after logic
		return true;
	}

}
