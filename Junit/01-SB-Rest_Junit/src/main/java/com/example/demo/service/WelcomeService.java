package com.example.demo.service;

import com.example.demo.model.User;

public interface WelcomeService 
{
	String getWelcomeMsg();
	
	
	boolean addUser(User u);
	
	boolean updateUser(User user);

}
