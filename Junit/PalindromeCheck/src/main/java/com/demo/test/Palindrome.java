package com.demo.test;

import java.util.Arrays;

public class Palindrome 
{
	
	public boolean isPalindrome(String name)
	{
		
		char[] c=name.toCharArray();
		char [] d=new char[c.length];
		int j=0;
		for(int i=name.length()-1;i>=0;i--)
		{
			
			d[j]=name.charAt(i);
			j++;
	     }
		
        boolean b=Arrays.equals(c, d);
		
		
		if(b)
		{
		 return true;
		}
		else
		{
		 return false;
		}
		
	
	}
	
}
