package com.antra;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Example1 {

	public static void main(String[] args) 
	{
		List<User> li=new ArrayList<>();
		User u1=new User(); u1.setUserId(123); u1.setUserRole("manager");
		User u2=new User(); u2.setUserId(222); u2.setUserRole("hr");
		User u3=new User(); u3.setUserId(789); u3.setUserRole("dev");
		li.add(u1); li.add(u2); li.add(u3);
		
		/*Set<String> s =li.stream().map(e->e.getUserRole()).collect(Collectors.toSet());
		System.out.println(s);*/
		Map<Integer,String> m=new HashMap<>();
		li.stream().forEach(e->m.put(e.getUserId(), e.getUserRole()));
		System.out.println(m.entrySet());
		
		
		

	}

}
