package com.antra;

public class Product 

{
	private Integer id;
	private Double cost;
	private String vendor;
	private String pname;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Double getCost() {
		return cost;
	}
	public void setCost(Double cost) {
		this.cost = cost;
	}
	public String getVendor() {
		return vendor;
	}
	public void setVendor(String vendor) {
		this.vendor = vendor;
	}
	public String getPname() {
		return pname;
	}
	public void setPname(String pname) {
		this.pname = pname;
	}
	@Override
	public String toString() {
		return "Product [id=" + id + ", cost=" + cost + ", vendor=" + vendor + ", pname=" + pname + "]";
	}
	
	
	
	
}
