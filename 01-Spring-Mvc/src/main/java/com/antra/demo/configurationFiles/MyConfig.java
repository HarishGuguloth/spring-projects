package com.antra.demo.configurationFiles;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@Configuration
@ComponentScan(basePackages="com.antra")
public class MyConfig {
	
	@Bean
	public ViewResolver viewResolver()
	{
		InternalResourceViewResolver ee=new InternalResourceViewResolver();
		
		ee.setPrefix("/");
		ee.setSuffix(".jsp");
		return ee;
	}

}
