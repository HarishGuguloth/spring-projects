package com.antra.demo.client;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.antra.demo.ClassWithDetails;

public class TesterClass {

	public static void main(String[] args) {
		ApplicationContext context=new ClassPathXmlApplicationContext("config.xml");
		ClassWithDetails detail=context.getBean("classWithDetails",ClassWithDetails.class);
		detail.outPut();

	}

}
