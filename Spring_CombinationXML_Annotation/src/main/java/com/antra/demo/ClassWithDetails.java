package com.antra.demo;

import org.springframework.beans.factory.annotation.Autowired;

public class ClassWithDetails 
{
	String myName;
	String myAge;
	
	@Autowired
	MessageClass messageClass;
	
	public void outPut() {
		String msg=messageClass.sayMessage();
		System.out.println(msg+"  user  "+myName+"  your age is "+myAge);
	}

	/*public ClassWithDetails(String myName, String myAge, MessageClass messageClass) {
	
		this.myName = myName;
		this.myAge = myAge;
		this.messageClass = messageClass;
	}*/

	public String getMyName() {
		return myName;
	}

	public String getMyAge() {
		return myAge;
	}

	public MessageClass getMessageClass() {
		return messageClass;
	}

	public void setMyName(String myName) {
		this.myName = myName;
	}

	public void setMyAge(String myAge) {
		this.myAge = myAge;
	}

	public void setMessageClass(MessageClass messageClass) {
		this.messageClass = messageClass;
	}
	
	
	

}
