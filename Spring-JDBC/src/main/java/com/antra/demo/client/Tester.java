package com.antra.demo.client;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.antra.demo.config.MyConfiguration;
import com.antra.demo.util.MyClass;

public class Tester {

	public static void main(String[] args) {
		
		ApplicationContext context=new AnnotationConfigApplicationContext(MyConfiguration.class);
		MyClass cls=context.getBean("myClass",MyClass.class);
		int i=cls.addEmployee();
		if(i==1) {
			System.out.println("record inserted....");
		}
		else
		{
			System.out.println("not inserted....");
		}

	}

}
