package com.antra.demo.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.antra.demo.entity.EmployeeEntity;


@Component
public class MyClass 
{
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	
	public int addEmployee()
	{
		EmployeeEntity ee=new EmployeeEntity();
	        ee.setEmpId(45600);
		ee.setEname("mahesh");
		ee.setSalary(90000.0);
		ee.setDeptNumber(90);
		int i=jdbcTemplate.update("insert into employee values (?,?,?,?)",ee.getEmpId(),ee.getEname(),ee.getSalary(),ee.getDeptNumber());
		return i;
	}

}
