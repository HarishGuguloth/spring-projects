package com.antra.demo.config;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@Configuration

@ComponentScan(basePackages="com")
public class MyConfiguration 
{
	@Bean
	public DataSource dataSource()
	{
		DriverManagerDataSource dd=new DriverManagerDataSource();
		dd.setDriverClassName("com.mysql.cj.jdbc.Driver");
		dd.setUrl("jdbc:mysql://localhost:3306/test");
		dd.setUsername("root");
		dd.setPassword("antra");
		return dd;
		
	}
	@Bean
	public JdbcTemplate jdbcTemplate()
	{
		JdbcTemplate jj=new JdbcTemplate(dataSource());
		return jj;
	}
	

}
