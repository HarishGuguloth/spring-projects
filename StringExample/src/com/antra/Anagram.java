package com.antra;

import java.util.Arrays;

public class Anagram {

	public static void main(String[] args) 
	{
		
		String s1="god";
		String s2="GOD";
		char[] c1=s1.toLowerCase().toCharArray();
		char[] c2=s2.toLowerCase().toCharArray();
		Arrays.sort(c1);
		Arrays.sort(c2);
		System.out.println(Arrays.equals(c1, c2));
		//int i=Arrays.compare(c1, c2);
		//System.out.println(i);
		

	}

}
