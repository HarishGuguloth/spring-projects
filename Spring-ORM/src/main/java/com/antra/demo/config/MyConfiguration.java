package com.antra.demo.config;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration

@ComponentScan(basePackages="com.antra")
@EnableTransactionManagement
public class MyConfiguration 
{
	@Bean
	public DataSource dataSource()
	{
		DriverManagerDataSource dd=new DriverManagerDataSource();
		dd.setDriverClassName("com.mysql.cj.jdbc.Driver");
		dd.setUrl("jdbc:mysql://localhost:3306/test");
		dd.setUsername("root");
		dd.setPassword("antra");
		return dd;
		
	}
	@Bean
	public HibernateJpaVendorAdapter hh()
	{
		 HibernateJpaVendorAdapter hiber=new  HibernateJpaVendorAdapter();
		 hiber.setGenerateDdl(true);
		 hiber.setShowSql(true);
	     return hiber;
		
	}
	@Bean
	public LocalContainerEntityManagerFactoryBean bb()
	{
		LocalContainerEntityManagerFactoryBean ll=new LocalContainerEntityManagerFactoryBean();
		ll.setDataSource(dataSource());
		ll.setJpaVendorAdapter(hh());
		ll.setPackagesToScan("com.antra");
		return ll;
		
	}
	@Bean
	public JpaTransactionManager jap()
	{
		 JpaTransactionManager  jj=new  JpaTransactionManager (bb().getObject());
		 return jj;
		
		
	}
	

}
