package com.antra.demo.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.antra.demo.dao.EmployeeDAO;
import com.antra.demo.entity.EmployeeEntity;

@Repository
public class EmployeeDAOImpl implements EmployeeDAO {
	
	@PersistenceContext
	EntityManager entityManager;

	@Override
	@Transactional
	public void insertDetails(EmployeeEntity ee) 
	{
		entityManager.persist(ee);
		
		

	}

}
