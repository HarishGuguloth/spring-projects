package com.antra.demo.client;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.antra.demo.config.MyConfiguration;
import com.antra.demo.dao.impl.EmployeeDAOImpl;
import com.antra.demo.entity.EmployeeEntity;

public class Tester {

	public static void main(String[] args) {
		
		ApplicationContext context=new AnnotationConfigApplicationContext(MyConfiguration.class);
		EmployeeDAOImpl emp=(EmployeeDAOImpl )context.getBean(EmployeeDAOImpl.class);
		EmployeeEntity ee=new EmployeeEntity();
		ee.setEmpId(987);
		ee.setEname("anvesh");
		ee.setSalary(35000.0);
		ee.setDeptNumber(30);
		emp.insertDetails(ee);
	}

}
