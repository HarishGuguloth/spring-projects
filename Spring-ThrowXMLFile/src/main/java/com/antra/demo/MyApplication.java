package com.antra.demo;

public class MyApplication 
{
	
	HelloWorld helloWorld;
	
	public void myMethod()
	{
		helloWorld.sayHello();
	}

	public void setHelloWorld(HelloWorld helloWorld) {
		this.helloWorld = helloWorld;
	}
	

}
