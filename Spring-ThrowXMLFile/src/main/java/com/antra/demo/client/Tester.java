package com.antra.demo.client;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.antra.demo.MyApplication;

public class Tester {

	public static void main(String[] args) {
	    
		ApplicationContext context=new ClassPathXmlApplicationContext("config.xml");
		MyApplication bean=context.getBean("myApplication", MyApplication.class);
		bean.myMethod();
		
		

	}

}
