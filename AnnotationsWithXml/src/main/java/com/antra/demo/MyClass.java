package com.antra.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class MyClass 
{
	@Autowired
	MyDetails myDetails;
	
	String myVillageName;
	
	String myPhoneNumber;
	
	
	public void myInfo()
	{
		myDetails.details();
		System.out.println("i am from "+myVillageName);
		System.out.println("my contact details are "+myPhoneNumber);
	}


	public MyDetails getMyDetails() {
		return myDetails;
	}


	public void setMyDetails(MyDetails myDetails) {
		this.myDetails = myDetails;
	}


	public String getMyVillageName() {
		return myVillageName;
	}


	public void setMyVillageName(String myVillageName) {
		this.myVillageName = myVillageName;
	}


	public String getMyPhoneNumber() {
		return myPhoneNumber;
	}


	public void setMyPhoneNumber(String myPhoneNumber) {
		this.myPhoneNumber = myPhoneNumber;
	}
	

}
