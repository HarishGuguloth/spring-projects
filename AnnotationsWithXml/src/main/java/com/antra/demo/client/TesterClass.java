package com.antra.demo.client;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.antra.demo.MyClass;

public class TesterClass {

	public static void main(String[] args)
	{
		
		ApplicationContext context=new ClassPathXmlApplicationContext("config.xml");
		MyClass cls=context.getBean("myClass",MyClass.class);
		cls.myInfo();

	}

}
