package com.antra.demo;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Sorted_Example {

	public static void main(String[] args)
	{
	   List<Employee> lstEmp=new ArrayList<>();
	   Employee e1=new Employee();
	   e1.setEmpId(123);
	   e1.setEmpName("harish");
	   e1.setDeptno(10);
	   e1.setDeptName("developer");
	   e1.setSalory(25000.0);
	   
	   Employee e2=new Employee();
	   e2.setEmpId(456);
	   e2.setEmpName("anvesh");
	   e2.setDeptno(20);
	   e2.setDeptName("testing");
	   e2.setSalory(43000.0);
	   
	   Employee e3=new Employee();
	   e3.setEmpId(789);
	   e3.setEmpName("suresh");
	   e3.setDeptno(30);
	   e3.setDeptName("salesforce");
	   e3.setSalory(2000.0);
	   Employee e4=new Employee();
	   e4.setEmpId(444);
	   e4.setEmpName("sanjay");
	   e4.setDeptno(30);
	   e4.setDeptName("hr");
	   e4.setSalory(12000.0);
	   Employee e5=new Employee();
	   e5.setEmpId(1111);
	   e5.setEmpName("santhosh");
	   e5.setDeptno(10);
	   e5.setDeptName("developer");
	   e5.setSalory(24000.0);
	   
	   lstEmp.add(e1); lstEmp.add(e2); lstEmp.add(e3); lstEmp.add(e4);lstEmp.add(e5);
	   Employee ee=lstEmp.stream().max((o1,o2)->o1.getSalory().compareTo(o2.getSalory())).get();
	 Employee eee=  lstEmp.stream().filter(e->e.getSalory()<ee.getSalory()).max((o1,o2)->o1.getSalory().compareTo(o2.getSalory())).get();
	 System.out.println(eee.getSalory()+" this is second highest salary");
	 
	   
	   /*
	   
	   List li=lstEmp.stream().filter(e-> e.getDeptno()==10).collect(Collectors.toList());
	   li.forEach(System.out::println);*/
	   
	  //salaries in decending order 
	   
	  /*List li= lstEmp.stream().sorted((e11,e22)->(int)(e22.getSalory()-e11.getSalory())).collect(Collectors.toList());
	   li.forEach(System.out::println);*/
	
	   
	   //increasing or decreasing the salaries using map method
	   
	/* List li= lstEmp.stream().map(e->
	 {
		 e.setSalory(e.getSalory()-3000);
		 return e;
		 
	 }).collect(Collectors.toList());
	 li.forEach(System.out::println);
	*/
	   
	   
	   
	  long i= lstEmp.stream().filter(e->e.getSalory()>22000).count();
	 System.out.println(i);
	 
	 
	boolean b= lstEmp.stream().anyMatch(e->e.getEmpName().equals("anvesh"));
	System.out.println(b);
	 
	
	//double min=lstEmp.stream().max((E1,E2)->(E1.getSalory().compareTo(E2.getSalory()))).get();
	
	//second heighes salary
	
	
	   
	   
	   
	   
	   

	}

}
