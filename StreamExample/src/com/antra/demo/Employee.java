package com.antra.demo;

public class Employee
{
	private Integer empId;
	private String empName;
	private Integer deptno;
	private String deptName;
	private Double salory;
	public Double getSalory()
	{
		return salory;
	}
	public void setSalory(Double salory)
	{
		this.salory=salory;
	}
	public Integer getEmpId() {
		return empId;
	}
	public void setEmpId(Integer empId) {
		this.empId = empId;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public Integer getDeptno() {
		return deptno;
	}
	public void setDeptno(Integer deptno) {
		this.deptno = deptno;
	}
	public String getDeptName() {
		return deptName;
	}
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
	@Override
	public String toString() {
		return "Employee [empId=" + empId + ", empName=" + empName + ", deptno=" + deptno + ", deptName=" + deptName
				+ ", salory=" + salory + "]";
	}
	
	

}
