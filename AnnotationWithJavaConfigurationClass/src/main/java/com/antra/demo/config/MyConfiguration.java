package com.antra.demo.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan(basePackages="com.antra")
@PropertySource("classpath:my.properties")
public class MyConfiguration 
{
	
	

}
