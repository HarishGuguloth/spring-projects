package com.antra.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class MyClass 
{
	@Autowired
	MySummary mySummary;
	
	@Value(value="24")
	String myAge;
	
	@Value("${vlg}")
	String myVilleage;
	public void methodOfClass()
	{
		mySummary.myDetails();
		System.out.println("you age is "+myAge);
		System.out.println("you are from "+myVilleage);
	}

	public MySummary getMySummary() {
		return mySummary;
	}

	public void setMySummary(MySummary mySummary) {
		this.mySummary = mySummary;
	}

	public String getMyAge() {
		return myAge;
	}

	public void setMyAge(String myAge) {
		this.myAge = myAge;
	}

	public String getMyVilleage() {
		return myVilleage;
	}

	public void setMyVilleage(String myVilleage) {
		this.myVilleage = myVilleage;
	}
	
	

}
