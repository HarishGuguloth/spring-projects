package com.antra.demo.client;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.antra.demo.MyClass;
import com.antra.demo.config.MyConfiguration;

public class TesterClass {

	public static void main(String[] args) {
		ApplicationContext context=new AnnotationConfigApplicationContext(MyConfiguration.class);
		MyClass cls=context.getBean("myClass", MyClass.class);
		cls.methodOfClass();

	}

}
